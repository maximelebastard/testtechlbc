# Test Android
leboncoin / Maxime Lebastard


## Objectifs

Ce projet est un test pour une candidature de développeur Android chez leboncoin.

Il est utilisé comme un support pour échanger autour du code, des choix d'achitecture et de l'état d'esprit du développeur.

Concrètement, il affiche une liste d'images chargées depuis un webservice dans une liste. Il doit
gérer l'absence de connexion.

L'API minimale supportée est l'API 14.

## Architecture

### Model-View-Presenter

L'application utilise une architecture MVP. Cela consiste en la séparation de la couche View (Activités, Fragments), 
la couche Presenter (logique métier) et la couche Model (modèle métier).

Bien qu'elle implique une certaine rigueur dans l'instanciation et la gestion du cycle de vie entre la vue et le presenter,
elle permet d'éviter de mélanger l'affichage et le métier au sein des activités/fragments et rend l'application plus
facilement testable.

### Dependency Injection

Afin de faciliter l'implémentation du MVP et la testabilité, l'application utilise Dagger 2 pour faire
de l'injection de dépendances.

### UI Binding

Le binding entre les layouts et la vue sont réalisés à l'aide de Butterknife afin de rendre le code plus lisible
et maintenable.

## Usage

Exécuter le projet depuis Android Studio. Les deux build variants par défaut sont équivalentes à ce stade du
projet.