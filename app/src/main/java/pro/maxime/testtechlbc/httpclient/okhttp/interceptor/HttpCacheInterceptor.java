package pro.maxime.testtechlbc.httpclient.okhttp.interceptor;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import pro.maxime.testtechlbc.util.NetworkUtil;

/**
 * OkHttp Cache Interceptor
 * Handles exceptions during requests by returning previously cached content.
 *
 * From http://mikescamell.com/gotcha-when-offline-caching-with-okhttp3/
 */
public class HttpCacheInterceptor implements Interceptor {

    private Context androidContext;

    public HttpCacheInterceptor(Context androidContext) {
        this.androidContext = androidContext;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        Request request = chain.request();

        if (!NetworkUtil.isNetworkConnected(this.androidContext))
        {
            CacheControl cacheControl = new CacheControl.Builder()
                    .maxStale( 1, TimeUnit.DAYS )
                    .build();

            request = request.newBuilder()
                    .cacheControl( cacheControl )
                    .build();
        }

        return chain.proceed(request);
    }

}
