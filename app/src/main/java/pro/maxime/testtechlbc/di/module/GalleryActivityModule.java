package pro.maxime.testtechlbc.di.module;

import dagger.Module;
import dagger.Provides;
import pro.maxime.testtechlbc.data.service.PhotosService;
import pro.maxime.testtechlbc.ui.GalleryActivity;
import pro.maxime.testtechlbc.ui.GalleryContract;
import pro.maxime.testtechlbc.ui.GalleryPresenter;

/**
 * Gallery activity module
 * Provides dependencies of the GalleryActivity
 */
@Module
public class GalleryActivityModule {

    @Provides
    GalleryContract.View provideView(GalleryActivity activity){
        return activity;
    }

    @Provides
    GalleryPresenter.Listener providePresenterListener(GalleryActivity activity) {
        return activity;
    }

    @Provides
    GalleryContract.Presenter providePresenter(GalleryContract.View view, GalleryPresenter.Listener listener, PhotosService photosService){
        return new GalleryPresenter(view, listener, photosService);
    }

}
