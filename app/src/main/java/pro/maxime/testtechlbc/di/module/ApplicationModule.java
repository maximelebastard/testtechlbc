package pro.maxime.testtechlbc.di.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pro.maxime.testtechlbc.ui.GalleryActivityComponent;

/**
 * Application scope module
 * Provides dependencies for the whole application lifecycle
 */
@Module( subcomponents = {GalleryActivityComponent.class })
public class ApplicationModule {

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

}
