package pro.maxime.testtechlbc.di.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import pro.maxime.testtechlbc.TestLbcApplication;
import pro.maxime.testtechlbc.di.module.ActivityBuilder;
import pro.maxime.testtechlbc.di.module.ApiModule;
import pro.maxime.testtechlbc.di.module.ApplicationModule;

/**
 * Application scope DI Component
 */
@Singleton
@Component( modules = {
        AndroidInjectionModule.class,
        ApplicationModule.class,
        ApiModule.class,
        ActivityBuilder.class})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }

    void inject(TestLbcApplication application);

}
