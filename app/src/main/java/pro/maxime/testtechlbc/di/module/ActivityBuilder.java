package pro.maxime.testtechlbc.di.module;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import pro.maxime.testtechlbc.ui.GalleryActivity;
import pro.maxime.testtechlbc.ui.GalleryActivityComponent;

/**
 * DI Activity Builder
 * Binds Android views to DI, allows Android views injection
 */
@Module
public abstract class ActivityBuilder {

    @Binds
    @IntoMap
    @ActivityKey(GalleryActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> bindMainActivity(GalleryActivityComponent.Builder builder);

}
