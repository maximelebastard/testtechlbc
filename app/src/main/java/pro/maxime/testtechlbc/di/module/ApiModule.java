package pro.maxime.testtechlbc.di.module;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import pro.maxime.testtechlbc.BuildConfig;
import pro.maxime.testtechlbc.data.service.PhotosService;
import pro.maxime.testtechlbc.httpclient.okhttp.interceptor.HttpCacheInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
public class ApiModule {

    @Provides
    @Singleton
    Cache providesOkHttpCache(Context context) {
        int cacheSize = 20 * 1024 * 1024; // 20 MiB
        return new Cache(context.getCacheDir(), cacheSize);
    }

    @Provides
    @Singleton
    OkHttpClient providesHttpClient(Context context, Cache okHttpCache) {
        return new OkHttpClient.Builder()
                .cache(okHttpCache)
                .addNetworkInterceptor(new StethoInterceptor())
                .addNetworkInterceptor(new HttpCacheInterceptor(context)) // TODO : Fix OkHttp caching and use it again
                                                                            // Cache does not work for the moment
                                                                            // (loading with connectivity -> airplane mode -> restart app -> loading error)
                                                                            // Need some investigations.
                .build();
    }

    @Provides
    Retrofit providesRetrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(BuildConfig.PHOTOS_API_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    @Provides
    PhotosService providesPhotoService(Retrofit retrofit) {
        return retrofit.create(PhotosService.class);
    }

}
