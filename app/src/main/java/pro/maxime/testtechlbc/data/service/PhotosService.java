package pro.maxime.testtechlbc.data.service;

import java.util.ArrayList;

import io.reactivex.Flowable;
import retrofit2.http.GET;

/**
 * REST Service providing Photos
 */
public interface PhotosService {

    @GET("photos")
    Flowable<ArrayList<PhotoItem>> list();

}
