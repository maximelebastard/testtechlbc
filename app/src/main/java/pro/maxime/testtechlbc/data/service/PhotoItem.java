package pro.maxime.testtechlbc.data.service;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;

/**
 * Photo item model as responded by the REST service
 */
public class PhotoItem implements Parcelable {

    protected Integer albumId;

    protected Integer id;

    protected String title;

    protected URL url;

    protected URL thumbnailUrl;

    public PhotoItem() {
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public URL getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(URL thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    @Override
    public String toString() {
        return "PhotoItem{" +
                "albumId=" + albumId +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", url=" + url +
                ", thumbnailUrl=" + thumbnailUrl +
                '}';
    }

    protected PhotoItem(Parcel in) {
        albumId = (Integer) in.readValue(Integer.class.getClassLoader());
        id = (Integer) in.readValue(Integer.class.getClassLoader());
        title = in.readString();
        url = (URL) in.readValue(URL.class.getClassLoader());
        thumbnailUrl = (URL) in.readValue(URL.class.getClassLoader());

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(albumId);
        dest.writeValue(id);
        dest.writeString(title);
        dest.writeValue(url);
        dest.writeValue(thumbnailUrl);
    }

    public static final Parcelable.Creator<PhotoItem> CREATOR = new Parcelable.Creator<PhotoItem>() {
        @Override
        public PhotoItem createFromParcel(Parcel in) {
            return new PhotoItem(in);
        }

        @Override
        public PhotoItem[] newArray(int size) {
            return new PhotoItem[size];
        }
    };
}
