package pro.maxime.testtechlbc.ui;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import pro.maxime.testtechlbc.di.module.GalleryActivityModule;

/**
 * GalleryActivityComponent
 * Provides a builder of GalleryActivity for DI
 */
@Subcomponent( modules = { GalleryActivityModule.class })
public interface GalleryActivityComponent extends AndroidInjector<GalleryActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<GalleryActivity>{}

}
