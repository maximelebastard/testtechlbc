package pro.maxime.testtechlbc.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import pro.maxime.testtechlbc.R;
import pro.maxime.testtechlbc.data.service.PhotoItem;

/**
 * Gallery view
 */
public class GalleryActivity extends AppCompatActivity implements GalleryContract.View, GalleryPresenter.Listener {

    private final static String TAG = "GalleryActivity";

    @Inject
    GalleryContract.Presenter presenter;

    /**
     * Photos data
     */
    private ArrayList<PhotoItem> photos = new ArrayList<>();

    /**
     * Photos ListView adapter
     */
    private ArrayAdapter<PhotoItem> listViewAdapter;

    @BindView(R.id.gallery_photos_list)
    ListView photosListView;

    /**
     * @inheritDoc
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gallery);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        this.listViewAdapter = new GalleryPhotosListAdapter(this, this.photos);
        this.photosListView.setAdapter(this.listViewAdapter);

    }

    /**
     * @inheritDoc
     */
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        this.presenter.start( savedInstanceState != null ? this.readStateFromBundle(savedInstanceState) : null );
    }

    /**
     * @inheritDoc
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        this.writeStateToBundle(outState, this.presenter.getState());
    }

    /**
     * @inheritDoc
     */
    protected void writeStateToBundle(Bundle bundle, GalleryContract.State state) {
        bundle.putParcelableArrayList(GalleryContract.State.BKEY_PHOTOS, state.getPhotos());
    }

    /**
     * @inheritDoc
     */
    protected GalleryContract.State readStateFromBundle(Bundle bundle) {
        GalleryContract.State state = new GalleryState();
        state.setPhotos(bundle.<PhotoItem>getParcelableArrayList(GalleryContract.State.BKEY_PHOTOS));

        return state;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onPhotosLoaded(ArrayList<PhotoItem> photoItems) {
        this.renderPhotos(photoItems);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onPhotosLoadingError(Throwable exception) {
        this.displayPhotosLoadingError();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void renderPhotos(ArrayList<PhotoItem> photos) {

        // TODO : In case of refreshing, improve this by using a HashMap instead of an ArrayList.
        // Key of the hashmap should be the photo id
        // This way, we can make a diff on the current vs the new dataset

        this.photos.clear();
        this.photos.addAll(photos);
        this.listViewAdapter.notifyDataSetChanged();

    }

    /**
     * @inheritDoc
     */
    @Override
    public void displayPhotosLoadingError() {
        Toast.makeText(this, getString(R.string.photos_loading_error), Toast.LENGTH_SHORT).show();
    }

    /**
     * @inheritDoc
     */
    @Override
    protected void onStop() {
        super.onStop();
        this.presenter.stop();
    }

    /**
     * @inheritDoc
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.presenter.destroy();
    }
}
