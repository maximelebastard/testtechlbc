package pro.maxime.testtechlbc.ui;

import android.support.annotation.Nullable;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import pro.maxime.testtechlbc.data.service.PhotoItem;
import pro.maxime.testtechlbc.data.service.PhotosService;

/**
 * Gallery presenter
 */
public class GalleryPresenter implements GalleryContract.Presenter {

    private final static String TAG = "GalleryPresenter";

    private GalleryContract.View view;
    private Listener listener;

    /**
     * Disposables handler
     */
    private CompositeDisposable disposables = new CompositeDisposable();

    /**
     * Photos API Service
     */
    private PhotosService photosService;

    private ArrayList<PhotoItem> photos;

    public GalleryPresenter(GalleryContract.View view, Listener listener, PhotosService photosService) {
        this.view = view;
        this.photosService = photosService;
        this.listener = listener;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void start(@Nullable GalleryContract.State state) {

        if(null != state && null != state.getPhotos()) {
            this.photos = state.getPhotos();
            this.listener.onPhotosLoaded(this.photos);
        } else {
            this.loadPhotos();
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void loadPhotos() {

        this.disposables.add(this.photosService.list()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Consumer<ArrayList<PhotoItem>>() {
                            @Override
                            public void accept(ArrayList<PhotoItem> photoItems) throws Exception {
                                GalleryPresenter.this.photos = photoItems;
                                GalleryPresenter.this.listener.onPhotosLoaded(GalleryPresenter.this.photos);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                throwable.printStackTrace();
                                GalleryPresenter.this.listener.onPhotosLoadingError(throwable);
                            }
                        }
                ));

    }

    /**
     * @inheritDoc
     */
    @Override
    public void stop() {
        this.disposables.clear();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void destroy() {
        this.disposables.clear();
    }

    @Override
    public GalleryContract.State getState() {
        GalleryContract.State state = new GalleryState();
        state.setPhotos(this.photos);

        return state;
    }

}
