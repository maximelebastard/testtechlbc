package pro.maxime.testtechlbc.ui;

import android.support.annotation.Nullable;

import java.util.ArrayList;

import pro.maxime.testtechlbc.data.service.PhotoItem;

/**
 * Gallery MVP contract
 * The gallery displays Photo items
 */
public interface GalleryContract {

    interface Presenter {

        /**
         * Presenter events listener
         */
        interface Listener {

            void onPhotosLoaded(ArrayList<PhotoItem> photoItems);
            void onPhotosLoadingError(Throwable exception);

        }

        /**
         * Called on view startup
         */
        void start(@Nullable State state);

        /**
         * Called on view interruption
         */
        void stop();

        /**
         * Called on view destroying
         */
        void destroy();

        /**
         * @return The current state
         */
        State getState();

        /**
         * Starts loading photos from the PhotosService
         */
        void loadPhotos();

    }

    interface State {
        String BKEY_PHOTOS = "BUNDLE_PHOTOS";

        ArrayList<PhotoItem> getPhotos();
        void setPhotos(ArrayList<PhotoItem> photos);
    }

    interface View {

        /**
         * Displays photos onto the view
         */
        void renderPhotos(ArrayList<PhotoItem> photos);

        /**
         * Shows an error message when photos loading fails
         */
        void displayPhotosLoadingError();

    }

}
