package pro.maxime.testtechlbc.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import pro.maxime.testtechlbc.R;
import pro.maxime.testtechlbc.data.service.PhotoItem;


public class GalleryPhotosListAdapter extends ArrayAdapter<PhotoItem> {

    public GalleryPhotosListAdapter(@NonNull Context context, @NonNull List<PhotoItem> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        PhotoItem item = this.getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.gallery_photos_list_item, parent, false);
        }

        if (item != null) {
            ImageView imageView = convertView.findViewById(R.id.gallery_photos_list_item_image);
            imageView.setContentDescription(item.getTitle());

            Picasso.with(this.getContext())
                    .load(item.getThumbnailUrl().toExternalForm())
                    .into(imageView);

            TextView titleTextview = convertView.findViewById(R.id.gallery_photos_list_item_title);
            titleTextview.setText(item.getTitle());
        }

        return convertView;
    }
}
