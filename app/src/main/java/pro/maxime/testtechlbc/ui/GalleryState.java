package pro.maxime.testtechlbc.ui;

import java.util.ArrayList;

import pro.maxime.testtechlbc.data.service.PhotoItem;

public class GalleryState implements GalleryContract.State {


    private ArrayList<PhotoItem> photos;

    @Override
    public ArrayList<PhotoItem> getPhotos() {
        return photos;
    }

    @Override
    public void setPhotos(ArrayList<PhotoItem> photos) {
        this.photos = photos;
    }
}
